import java.io.IOException;

/**
 * Created by Erick on 26/06/2017.
 */
public class Main {
    public static void main (String args[]){
        Object obj = new Object();
        Fruit fruit = new Fruit();
        Apple apple = new Apple();
        Citrus citrus = new Citrus();
        Orange orange = new Orange();
        Squeezable squeezable;








        try {
           castFruitApple(fruit, apple);
        }
        catch(CastException e) {
            System.out.print(e.getMessage());
        }
    }

    //Object
    public static void castObjectFruit(Object object, Fruit fruit) throws CastException{
        if (object instanceof Fruit) {
            fruit = (Fruit) object;
            System.out.println("Fruit: OK");
        }
        else {
            throw new CastException("Fruit: NO");
        }
    }

    public static void castObjectApple(Object object, Apple apple  ) throws CastException{
        if (object instanceof Apple) {
            apple = (Apple) object;
            System.out.println("Apple: OK");
        }
        else {
            throw new CastException("Apple: NO");
        }
    }

    public static void castObjectSqueezable(Object object, Squeezable squeezable  ) throws CastException{
        if (object instanceof Squeezable) {
            squeezable = (Squeezable) object;
            System.out.println("Squeezable: OK");
        }
        else {
            throw new CastException("Squeezable: NO");
        }
    }

    public static void castObjectCitrus(Object object, Citrus citrus) throws CastException{
        if (object instanceof Citrus) {
            citrus = (Citrus) object;
            System.out.println("Citrus: OK");
        }
        else {
            throw new CastException("Citrus: NO");
        }
    }

    public static void castObjectOrange(Object object, Orange orange) throws CastException{
        if (object instanceof Orange) {
            orange = (Orange) object;
            System.out.println("Orange: OK");
        }
        else {
            throw new CastException("Orange: NO");
        }
    }


    //Fruit
    public static void castFruitApple(Fruit fruit, Apple apple  ) throws CastException{
        if (fruit instanceof Apple) {
                apple = (Apple) fruit;
                System.out.println("Apple: OK");
            }
        else {
                throw new CastException("Apple: NO");
            }
    }

    public static void castFruitSqueezable(Fruit fruit, Squeezable squeezable  ) throws CastException{
        if (fruit instanceof Squeezable) {
            squeezable = (Squeezable) fruit;
            System.out.println("Squeezable: OK");
        }
        else {
            throw new CastException("Squeezable: NO");
        }
    }

    public static void castFruitCitrus(Fruit fruit, Citrus citrus) throws CastException{
        if (fruit instanceof Citrus) {
            citrus = (Citrus) fruit;
            System.out.println("Citrus: OK");
        }
        else {
            throw new CastException("Citrus: NO");
        }
    }

    public static void castFruitOrange(Fruit fruit, Orange orange) throws CastException{
        if (fruit instanceof Orange) {
            orange = (Orange) fruit;
            System.out.println("Orange: OK");
        }
        else {
            throw new CastException("Orange: NO");
        }
    }

    //Apple
    public static void castAppleFruit(Apple apple, Fruit fruit  )throws CastException{
        if (apple instanceof Fruit) {
            fruit = (Fruit) apple;
            System.out.println("Apple: OK");
        }
        else {
            throw new CastException("Apple: NO");
        }
    }

    public static void castAppleSqueezable(Apple apple, Squeezable squeezable  ) throws CastException{
        if (apple instanceof Squeezable) {
            squeezable = (Squeezable) apple;
            System.out.println("Squeezable: OK");
        }
        else {
            throw new CastException("Squeezable: NO");
        }
    }

    public static void castAppleCitrus(Apple apple, Citrus citrus) throws CastException{
        if (apple.getClass().isInstance(citrus)) {
            System.out.println("Citrus: OK");
        }
        else {
            throw new CastException("Citrus: NO");
        }
    }

    public static void castAppleOrange(Apple apple, Orange orange) throws CastException{
        if (apple.getClass().isInstance(orange)) {
            System.out.println("Orange: OK");
        }
        else {
            throw new CastException("Orange: NO");
        }
    }

    //Citrus
    public static void castCitrusFruit(Citrus citrus, Fruit fruit  ) throws CastException{
        if (citrus instanceof Fruit) {
            fruit = (Fruit) citrus;
            System.out.println("Citrus: OK");
        }
        else {
            throw new CastException("Citrus: NO");
        }
    }

    public static void castCitrusApple(Citrus citrus, Apple apple) throws CastException{
        if (citrus.getClass().isInstance(apple) ) {
            System.out.println("Apple: OK");
        }
        else {
            throw new CastException("Apple: NO");
        }
    }

    public static void castAppleSqueezable(Citrus citrus, Squeezable squeezable  ) throws CastException{
        if (citrus instanceof Squeezable) {
            squeezable = (Squeezable) citrus;
            System.out.println("Squeezable: OK");
        }
        else {
            throw new CastException("Squeezable: NO");
        }
    }

    public static void castAppleOrange(Citrus citrus, Orange orange) throws CastException{
        if (citrus instanceof Orange) {
            orange = (Orange) citrus;
            System.out.println("Orange: OK");
        }
        else {
            throw new CastException("Orange: NO");
        }
    }

}
